"""
Generate fastapi server with an endpoint to return hello world and start the server
"""


def hello_world(request):
    return "Hello World!"


def main():
    from fastapi import FastAPI
    app = FastAPI()
    app.add_route(hello_world, "/")
    app.run(host="<ipv4 placeholder|x.x.x.x>", port=800)
    